#!/bin/sh

set -e

DIR=$(dirname ${0})
SOURCES_FILE="${DIR}/sources.txt"
PATCHES_FILE="${DIR}/srouces-patches.sed"

BASE=
for I in $(grep -v "^#" "${SOURCES_FILE}"); do
	if [ "${I#BASE=}" != "${I}" ]; then
		BASE="${I#BASE=}"
	else
		mkdir -vp "${DIR}/$(dirname ${I})"
		wget -O "${DIR}/${I}" "${BASE}${I}"
		sed -f "${PATCHES_FILE}" -i "${DIR}/${I}"
		if [ "${I%.sh}" != "${I}" ]; then
			chmod -v 755 "${DIR}/${I}"
		fi
	fi
done
